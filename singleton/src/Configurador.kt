package com.singleton

class Configurador(url: String, baseDatos: String)  {

    val url = url
    val baseDatos = baseDatos

    final var miConfigurador: Configurador? = null

    fun getConfigurador(url: String, baseDatos: String ): Configurador? {
        if (this.miConfigurador== null) {
            miConfigurador = Configurador(url, baseDatos);
        }

        return miConfigurador;

    }

    fun prueba(): String
    {
        return "hola"
    }

}